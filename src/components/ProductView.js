/*IMPORT FROM PACKAGES*/
import {Container, Row, Col, Card, Button, Form} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';

/*IMPORT FROM SRC*/
import UserContext from '../UserContext';

import ProductUpdate from './ProductUpdate';


/**********************************************/

export default function ProductView(){


	const {user} = useContext(UserContext);
	const {productId} = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState();

	const history = useNavigate();
	const totalAmount = price * Number(quantity);


	// [SECTION] FOR ORDER CREATION
	const addToCart = (productId) => {

		// [SECTION] prevent page redirection via form submission
		// e.preventDefault();

	fetch(`https://salty-basin-97262.herokuapp.com/orders/createOrder/${productId}`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity : quantity,
				totalAmount: totalAmount
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data){
				Swal.fire({
					title: 'Successful Purchase',
					icon: 'success',
					text: 'Thank you for purchasing!'
				});
				history("/products");
			}else{
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				})
			}
		})
	};

	// [SECTION] UPDATING PRODUCT

	useEffect (() => {
		fetch(`https://salty-basin-97262.herokuapp.com/products/singleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		});
	}, [productId]);

	
	const isAdmin = user.isAdmin;
	return(
		<>
			{(isAdmin!==true)?
			
			<Container className="mt-5">
				<Row>
					<Col lg={{span:6, offset:3}}>
						<Card>
							<Card.Body>
								<Card.Title>{name}</Card.Title>
								<Card.Subtitle>Description:</Card.Subtitle>
								<Card.Text>{description}</Card.Text>
								<Card.Subtitle>Price:</Card.Subtitle>
								<Card.Text>Php {price}</Card.Text>
								<Card.Subtitle>Quantity:</Card.Subtitle>
								<Form className="quantityForm">
									<Form.Group className="mb-3">
									<Form.Control 
									type="number" 
									id="#quantity-amount"
									value={quantity}
									onChange={productId => setQuantity(productId.target.value)}
									required
									/>
									</Form.Group>
								</Form>
								<hr/>
								{ user.id !== null?
									<Button className="generalBtn" onClick={() => addToCart(productId)}>Buy Now</Button>
									:
									<Link className="generalBtn btn btn-danger" to="/login">Log in</Link>
								}
								<Card.Text className="total">PHP {totalAmount}.00</Card.Text>
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>

			:

			<ProductUpdate/>
			}
		</>
	)
} 
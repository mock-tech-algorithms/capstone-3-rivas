/*IMPORT FROM PACKAGES*/
import {Container, Row, Col, Card, Button, Form, Modal} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';

/*IMPORT FROM SRC*/
import UserContext from '../UserContext';
/**********************************************/

export default function ProductUpdate(){

	// [SECTION] FOR HANDLING USERS DATA 
	const {user} = useContext(UserContext);
	const history = useNavigate();

	// [SECTION] FOR FETCHING ALL PRODUCTS BY ADMIN
	const {productId} = useParams();
	const {_id} = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState();

	// [SECTION] FOR MODAL
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	// [SECTION] ADD NEW PRODUCT BY ADMIN
	const updateProductDetails = (_id) =>{
		

		fetch(`https://salty-basin-97262.herokuapp.com/products/updateProductInformation/${productId}`, {
			method: 'PUT',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data){

				// [SECTION] Clear fields
				setName('');
				setDescription('');
				setPrice('');

				Swal.fire({
					title: 'Product successfully updated',
					icon: 'success',
					text: 'You can add more'
				});
			//history("/products");
			}else{
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				})
			}
		})
	};

	// [SECTION] UPDATING PRODUCT

	useEffect (() => {
		fetch(`https://salty-basin-97262.herokuapp.com/products/singleProduct/${productId}`)
		.then(res => res.json())
		.then(data => {
			//console.log(data)
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		});
	}, [productId]);

	return(
		<>
		<Card className="prodUpdate bg-info bg-opacity-25">
		<Card.Body>
			<Card.Text>{name}</Card.Text>
			<Card.Text>{description}</Card.Text>
			<Card.Text>{price}</Card.Text>
			<hr/>
			<Button className="generalBtn" onClick={handleShow}>Update product details</Button>
			<Modal show={show} onHide={handleClose}>
				
				<Modal.Header closeButton>
					<Modal.Title>Update Product Details</Modal.Title>
				</Modal.Header>
				
				<Modal.Body>
					<Form onSubmit = {(e) => updateProductDetails(e)}>
						<Form.Group className="mb-3" controlId="productName">
							<Form.Label>Product Name:</Form.Label>
							<Form.Control
								type="text"
								placeholder="ex. Cartoons design To-Do-List"
								required
								value={name}
								onChange={_id => setName(_id.target.value)}
								autoFocus
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control 
								as="textarea" 
								rows={2} 
								required
								value={description}
								onChange={_id => setDescription(_id.target.value)}
							/>
			            </Form.Group>

						<Form.Group className="mb-3" controlId="productPrice">
							<Form.Label>Price:</Form.Label>
							<Form.Control
								type="number"
								placeholder="ex. 10"
								required
								value={price}
								onChange={_id => setPrice(_id.target.value)}
							/>
						</Form.Group>
				       
							<>
								<Button variant="secondary" onClick={handleClose}>
									Close
								</Button>
								<Button variant="primary" type="submit" id="submitBtn" onClick={() => updateProductDetails(_id), handleClose} >

									Save Changes
								</Button>

							</>
				    </Form>
				</Modal.Body>
			</Modal>
		</Card.Body>
		</Card>
		</>
	)
}
/*IMPORT FROM PACKAGES*/
import {useState, useEffect, useContext} from 'react';
import {Col, Card, Button, Row} from 'react-bootstrap';
import {Link, useParams, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

/*IMPORT FROM SRC*/
import UserContext from '../UserContext';

/**********************************************/

export default function ProductCard({productProp}){

	const {user} = useContext(UserContext);
	const history = useNavigate();

	// object destructuring
	const {name, description, price, isActive, _id} = productProp;


	const {productId} = useParams();
	// [SECTION] FOR ARCHIVING/SOFT DELETION OF ACTIVE PRODUCT
		const archiveProduct= (_id) => {
			fetch(`https://salty-basin-97262.herokuapp.com/products/archiveProduct/${_id}`,{
				method: "DELETE",
				headers: {
					'Content-Type' : 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				if(data){
					console.log(data)
					Swal.fire({
						title: 'Product was successfully deteled',
						icon: 'success',
						text: ''
					});
					window.location.reload(true);	
					//history("/products");
				}else{
					console.log(data)
					Swal.fire({
						title: 'Something went wrong',
						icon: 'error',
						text: 'Please try again later'
					})
				}
			})
		}

		// [SECTION] TO SET INACTIVE PRODUCT TO ACTIVE
		const setProductActive = (_id) => {
			fetch(`https://salty-basin-97262.herokuapp.com/products/activateProduct/${_id}`,{
				method: "PUT",
				headers: {
					'Content-Type' : 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				if(data){
					console.log(data)
					Swal.fire({
						title: 'Product was successfully Activate',
						icon: 'success',
						text: ''
					});
					window.location.reload(true);	
					//history("/products");
				}else{
					console.log(data)
					Swal.fire({
						title: 'Something went wrong',
						icon: 'error',
						text: 'Please try again later'
					})
				}
			})
		}
	return(
		<Row>
			<Col xs={12}  className="mt-4">
				<Card className="grid-container p-3 mb-3">
					<Card.Body>
						<Card.Title>
							<h4>{name}</h4>
						</Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>
							{description}
						</Card.Text>
						<Card.Subtitle>Price:</Card.Subtitle>
						<Card.Text>{price}</Card.Text>
						<hr/>
						{(user.isAdmin!==true)?
						<Link className="generalBtn btn" to={`/productView/${_id}`}>View Product Details</Link>
						:
						<>
						<Link className="generalBtn btn" to={`/productView/${_id}`}>Update Product Details</Link>
							
							{(isActive===false)?
								
							<Button className="generalBtn"  onClick={() => setProductActive(_id)}>Activate</Button>
							:
							<Button className="generalBtn" onClick={() => archiveProduct(_id)}>Delete</Button>

							}
							
						</>
						}
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
};
import {Figure, Row, Image, Carousel} from 'react-bootstrap';
import note1 from '../images/note1.gif';
import note2 from '../images/note2.gif';
import note3 from '../images/note3.gif';

export default function ProductCard(){
	return(

		<Carousel classNanoteme="productBanner "variant="dark">
		     <Carousel.Item className="bannerItem">
		       <img
		         className="d-block w-100"
		         src={note1}
		         alt="First slide"
		       />
		     </Carousel.Item>
		     <Carousel.Item>
		       <img
		         className="d-block w-100"
		         src={note2}
		         alt="Second slide"
		       />
			</Carousel.Item>
		     <Carousel.Item>
		       <img
		         className="d-block w-100"
		         src={note3}
		         alt="Third slide"
		       />
		     </Carousel.Item>
		   </Carousel>  
	)
}
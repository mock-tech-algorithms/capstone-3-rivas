/*IMPORT FROM PACKAGE*/
import {Link} from 'react-router-dom';
import {Navbar, Container, Nav} from 'react-bootstrap';
import {useState, useContext} from 'react';

/*IMPORT FROM SRC*/
import UserContext from '../UserContext';

/**********************************************/

export default function ApplicationNavbar(){

	const {user} = useContext(UserContext);

	return(
		<Navbar className="navbar" expand="lg">
			<Container>
				<Navbar.Brand as={Link} to="/">TakeNOTE</Navbar.Brand>
		        	<Navbar.Toggle aria-controls="basic-navbar-nav"/>
						<Navbar.Collapse id="basic-navbar-nav">
							<Nav className="me-auto">
								<Nav.Link className="nav" as={Link} to="/">Home</Nav.Link>
								<Nav.Link className="nav" as={Link} to="/products">Products</Nav.Link>
								{
									(user.id !== null) ?
									<>
										<Nav.Link className="nav" as={Link} to="/profile">Profile</Nav.Link>
										<Nav.Link className="nav" as={Link} to="/logout">Logout</Nav.Link> 
									</>
									:
									<>
										<Nav.Link className="nav" as={Link} to="/login">Login</Nav.Link>
										<Nav.Link className="nav" as={Link} to="/register">Register</Nav.Link>
									</>
								}
		          			</Nav>
						</Navbar.Collapse>			
			</Container>
		</Navbar>

	)
};
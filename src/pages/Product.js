
/*IMPORT FROM PACKAGES*/
import {Row, Button, Form, Modal} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';

/*IMPORT FROM SRC*/
import ProductBanner from "../components/ProductBanner";
import ProductCard from "../components/ProductCard";
import ProductView from '../components/ProductView';

/*IMPORT FROM SRC*/
import UserContext from '../UserContext';
/**********************************************/

export default function Products(){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);

	// set Submit button default: false
	const [isActive, setIsActive] = useState(false);

	// [SECTION] FOR MODAL
	const [show, setShow] = useState(false);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	// [SECTION] FOR FETCHING/HANDLING USERS DATA
	const {user} = useContext(UserContext);
	const history = useNavigate();

	//[SECTION] FOR FETCHING DATA FROM DB(by user)
	const [product, setProduct] = useState([]);
	const columnPerRow = 2;

	//[SECTION] FOR FETCHING DATA FROM DB(by admin)
	const [products, setProducts] = useState([]);
	const columnsPerRow = 2;


	// [SECTION] FOR FETCHING ALL ACTIVE PRODUCTS
	useEffect(() => {
		fetch("https://salty-basin-97262.herokuapp.com/products/activeProducts")
		.then(res => res.json())
		.then(data => {
			const productArr = setProduct(data.map(product => {
				return(
				<ProductCard key={product._id} productProp = {product}/>
				)
			}))
		})
	}, []);

	// [SECTION] FOR FETCHING ALL PRODUCTS BY ADMIN
	useEffect(() => {
		fetch("https://salty-basin-97262.herokuapp.com/products/getAllProducts")
		.then(res => res.json())
		.then(data => {
			const productArr = setProducts(data.map(products => {
				return(
				<ProductCard key={products._id} productProp = {products}/>
				)
			}))
		})
	}, []);


	// [SECTION] ADD NEW PRODUCT BY ADMIN
	function addNewProduct(e){
		e.preventDefault();

		fetch(`https://salty-basin-97262.herokuapp.com/products/`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data){

				// [SECTION] Clear fields
				setName('');
				setDescription('');
				setPrice('');

				Swal.fire({
					title: 'Product successfully added',
					icon: 'success',
					text: 'You can add more'
				});
			//history("/products");
			}else{
				Swal.fire({
					title: 'Something went wrong',
					icon: 'error',
					text: 'Please try again later'
				})
			}
		})
	};

	useEffect(() => {
		if((name !== "" && description !== "" && price !=="")){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	}, [name, description, price]);

	// [SECTION] DECLARING USER.ISADMIN AS ISADMIN
	const isAdmin = user.isAdmin;


	return(

		<>
			<ProductBanner/>

				{(isAdmin!==true)?
					<>
						<h1>Available Products:</h1>	
							<Row md={columnPerRow}>
								{product}
							</Row>	
					</>
				:
					<>
						<Button className="generalBtn" onClick={handleShow}>Add new product</Button>
						<Modal show={show} onHide={handleClose}>
						        <Modal.Header closeButton>
						          <Modal.Title>Add new product</Modal.Title>
						        </Modal.Header>
						        <Modal.Body>
						          <Form onSubmit = {(e) => addNewProduct(e)}>
						            <Form.Group className="mb-3" controlId="productName">
						              <Form.Label>Product Name:</Form.Label>
						              <Form.Control
						                type="text"
						                placeholder="ex. Cartoons design To-Do-List"
						                required
						                value={name}
						                onChange={e => setName(e.target.value)}
						                autoFocus
						              />
						            </Form.Group>

						            <Form.Group
						              className="mb-3"
						              controlId="productDescription"
						            >
						              <Form.Label>Description</Form.Label>
						              <Form.Control 
						              as="textarea" 
						              rows={2} 
						              required
						              value={description}
						              onChange={e => setDescription(e.target.value)}
						              />
						            </Form.Group>

						            <Form.Group className="mb-3" controlId="productPrice">
						              <Form.Label>Price:</Form.Label>
						              <Form.Control
						                type="number"
						                placeholder="ex. 10"
						                required
						                value={price}
						                onChange={e => setPrice(e.target.value)}
						              />
						            </Form.Group>
						       
						        {
						        	isActive?
						        	<>
						          <Button variant="secondary" onClick={handleClose}>
						            Close
						          </Button>
						          <Button variant="primary" type="submit" id="submitBtn">
						            Save Changes
						          </Button>
						          </>
						          :
						          <>
						          <Button variant="secondary" onClick={handleClose}>
						            Close
						          </Button>
						          <Button variant="primary" type="submit" id="submitBtn" disabled>
						            Save Changes
						          </Button>
						          </>
						        
						      }
						     		</Form>
						        </Modal.Body>
						      </Modal>

						<h1>Available Products:</h1>	
							<Row md={columnsPerRow}>
								{products}
							</Row>	
					</>
				}
		
		</>
	);
};

/*IMPORT FROM PACKAGE*/
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import {useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2';

/*IMPORT FROM SRC*/
import UserContext from '../UserContext';

/**********************************************/

export default function Register() {

	const {user, setUser} = useContext(UserContext);
	const history = useNavigate();

	// [SECTION] Hold/stored the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isAdmin, setIsAdmin] = useState(false);

	// [SECTION] set Submit button default: false
	const [isActive, setIsActive] = useState(false);

	function registerUser(e){

		// [SECTION] prevent page redirection via form submission
		e.preventDefault();

		// [SECTION] Check if the email is existing > Yes = popup message > NO = proceed to registering
		fetch('https://salty-basin-97262.herokuapp.com/users/checkEmailExists', {
			method:"POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email:email
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data){
				Swal.fire({
					title: "This email already exist",
					icon: "info",
					text: "The email that you're trying to register already exist, try another"
				})
			}else{
				fetch('https://salty-basin-97262.herokuapp.com/users', {
					method: "POST",
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email:email,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					if (data.email){
						Swal.fire({
							title: "You're Registered!",
							icon: 'success',
							text: "Thank you for registering, you can log in now."
						});
						history("/login");
					}else{
						Swal.fire({
							title: 'Registration failed',
							icon: 'error',
							text: 'Something went wrong, try again'
						});
					};
				});
			};
		});

		// [SECTION] TO CLEAR INPUT FIELDS
		setEmail('');
		setPassword('');
		setFirstName('');
		setLastName('');
		setMobileNo('');
	};

	// [SECTION] VALIDATION FOR ALL REQUIRED FIELDS SHOULD NOT NULL
	useEffect(() => {
		if((email !== '' && password !== '' && firstName !== '' && lastName !== '' && mobileNo !== '' && mobileNo.length === 11)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}

	}, [email, password, firstName, lastName, mobileNo]);

	return(
		(user.id !== null) ?
			<Navigate to="/"/>
		:
		<>
		<h1>Register Here</h1>
		<Form className="registerForm d-block w-100" onSubmit={e => registerUser(e)}>
			<Form.Group className="mt-3" controlId="userFirstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
				className="registerFormControl"
				type="text"
				placeholder="Enter your first name here"
				required
				value={firstName}
				onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>
			<Form.Group className="mt-3" controlId="userLastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
				className="registerFormControl"
				type="text"
				placeholder="Enter your last name here"
				required
				value={lastName}
				onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>
			<Form.Group className="mt-3" controlId="userMobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
				className="registerFormControl"
				type="text"
				placeholder="Enter your 11-digit mobile number here"
				required
				value={mobileNo}
				onChange={e => setMobileNo(e.target.value)}
				/>
			</Form.Group>
			<Form.Group className="mt-3" controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
				className="registerFormControl"
				type="email"
				placeholder="Enter your email here"
				required
				value={email}
				onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="registerFormControl text-muted">
					We'll never share your email with anyone else
				</Form.Text>
			</Form.Group>

			<Form.Group className="mt-3" controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
				className="registerFormControl"
					type="password"
					placeholder="Enter your password here"
					required
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</Form.Group>
			{ isActive ?
				<Button className="registerFormBtn mt-3 mb-3" variant="success" type="submit" id="submitBtn">
					Register
				</Button>

				:

				<Button className="registerFormBtn mt-3 mb-3" variant="danger" type="submit" id="submitBtn" disabled>
					Register
				</Button>
			}
		</Form>
		</>
	)
}
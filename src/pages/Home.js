import Banner from '../components/Banner';
import {Card, Row, Col} from 'react-bootstrap';

import homeImg1 from '../images/homeImg1.png';
import homeImg2 from '../images/homeImg2.png';
import homeImg3 from '../images/homeImg3.png';
import homeImg4 from '../images/homeImg4.gif';
import homeImg5 from '../images/homeImg5.png';
import homeImg6 from '../images/homeImg6.png';
/**********************************************/

export default function Home(){

	return(
		<>
			<Banner/>
			
			<Row className="homeCard">
			<Col className="colCard"xs={12} md={4}>
				<Card>
					<Card.Img src={homeImg1}/>
				</Card>
			</Col>

			<Col className="colCard" xs={12} md={4}>
				<Card>
					<Card.Img src={homeImg2}/>
				</Card>
			</Col>

			<Col className="colCard"  xs={12} md={4}>
				<Card>
					<Card.Img src={homeImg3}/>
				</Card>
			</Col>
			</Row>
			
			<Row className="homeFigure mb-5">
			<Col className="colCard"  xs={12} md={4}>
				<Card>
					<Card.Img src={homeImg4}/>
				</Card>
			</Col>

			<Col className="colCard"  xs={12} md={4}>
				<Card>
					<Card.Img src={homeImg5}/>
				</Card>
			</Col>

			<Col className="colCard"  xs={12} md={4}>
				<Card>
					<Card.Img src={homeImg6}/>
				</Card>
			</Col>
			</Row>
		</>
	)
};
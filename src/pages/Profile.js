/*IMPORT FROM PACKAGES*/

import {useState, useEffect, useContext} from 'react';

/*IMPORT FROM COMPONENTS*/

import ProfileBody from "../components/ProfileBody";


/**********************************************/

export default function Profile(){



	const [user, setUsers] = useState([]);
	const isAdmin = user.isAdmin;

	useEffect(() => {
		fetch("https://salty-basin-97262.herokuapp.com/users/getUserDetails", {
		method: "GET",
		headers: {
	      Authorization: `Bearer ${localStorage.getItem('token')}`
	    }
	})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			const userArr = setUsers([data].map(user => {
				return(
					<ProfileBody key={user._id} userProp = {user}/>
				)
			}))
		})
	
	}, []);



	return(

		<>

		<h1>Account Settings:</h1>
		{user}
		</>

		
	)
}
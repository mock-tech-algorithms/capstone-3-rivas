import{BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {Container} from 'react-bootstrap';
import {useState, useEffect} from 'react';

/*IMPORT FROM COMPONENTS*/
import ApplicationNavbar from './components/ApplicationNavbar';

/*IMPORT FROM PAGES*/
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Product';
import ProductView from './components/ProductView';
import Profile from './pages/Profile';
import Register from './pages/Register';

/*IMPORT FROM SRC*/
import './App.css';
import {UserProvider} from './UserContext';

/**********************************************/

function App() {

	const [user, setUser] = useState({
	  id: null,
	  isAdmin: null
	});

	const unsetUser = () => {
	  localStorage.clear();
	};

	useEffect(() =>{
	  fetch('https://salty-basin-97262.herokuapp.com/users/getUserDetails', {
	    headers: {
	      Authorization: `Bearer ${localStorage.getItem('token')}`
	    }
	  })
	  .then(res => res.json())
	  .then(data => {
	    // captured the data of whoever is logged in
	    console.log(data);

	    if (typeof data._id !== "undefined"){
	        setUser({
	          id: data._id ,
	          isAdmin: data.isAdmin
	        });
	    }else{
	      // set back the initial state of user
	      setUser({
	        id: null,
	        isAdmin: null
	      });
	    };
	  });
	}, []);

	return(
	
		<>
		  <UserProvider value={{user, setUser,unsetUser}}>
		    <Router>
		      <ApplicationNavbar/>
		      <Container>
		        <Routes>
		          <Route exact path="/" element={<Home/>}/>
		          <Route exact path="/register" element={<Register/>}/>
		          <Route exact path="/login" element={<Login/>}/>
		          <Route exact path="/logout" element={<Logout/>}/>
		          <Route exact path="/products" element={<Products/>}/>
		          <Route exact path="/productView/:productId" element={<ProductView/>}/>
		          <Route exact path="/profile" element={<Profile/>}/>
		        </Routes>
		      </Container>
		    </Router>
		   </UserProvider>
		</>

	)
}

export default App;
